function staircase(n) {
    
    // Mendeklarasikan variabel yang akan menampung hasil akhir yang akan kita cetak
    let output = '';
    
    // Perulangan untuk mencetak semua baris
    for (let i = 1; i <= n; i++) {
        
        // Perulangan untuk mencetak spasi sebelumnya
        for (let j = n - 1; j >= i; j--) {
            output += ' ';
        }

        // Perulangan untuk mencetak # disetiap baris
        for (let k = 1; k <= i; k++) {
            output += '#'
        }

        // baris baru
        output += "\n";
       
    }
    // Menjalankan Output
    return output;
}

console.log(staircase(6));