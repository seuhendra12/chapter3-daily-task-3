function simpleArraySum(ar) {
    // Deklarasi variabel sum
    var sum = 0;

    // Perulangan untuk menjumlahkan angka dalam array
    for (var i = 0; i < ar.length; i++) {

        // Kondisi jika arraynya merupakan number
        if (typeof ar[i] == `number`) {

            // Penjumlahan array perIndex
            sum += ar[i];

        }

    }

    return sum;
}
console.log(simpleArraySum([1, 2, 3, 4]));