function aVeryBigSum(ar) {
    // Inisialisasi variabel sum dengan 0
    let sum = 0;

    // Perulangan untuk menjumlahkan angka didalam array
    for(let i = 0; i < ar.length; i++){

        // Penjumlahan angka perIndex
        sum += ar[i];

    }

    return sum;

}

console.log(aVeryBigSum([1000000001, 1000000002, 1000000003, 1000000004, 1000000005]));